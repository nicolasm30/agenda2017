<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
 
 
/**
 * Smarty utf8_encode modifier plugin
 *
 * Type:     modifier<br>
 * Name:     utf8_encode<br>
 * Purpose:  convert string utf-8
 * @author   Eric POMMEREAU
 * @param string
 * @return string
 */

function smarty_modifier_encripta($string)
{	
	$k;
	$clave="Cm2010#..$";
	$cifrado =MCRYPT_RIJNDAEL_256;
	$modo =MCRYPT_MODE_ECB;
	$cadenaen=mcrypt_encrypt($cifrado, $clave, $string, $modo,mcrypt_create_iv(mcrypt_get_iv_size($cifrado, $modo), MCRYPT_RAND));
	$cadenaen=base64_encode($cadenaen);
	$cadenaen=bin2hex($cadenaen);
	return $cadenaen;
}
 
?>
