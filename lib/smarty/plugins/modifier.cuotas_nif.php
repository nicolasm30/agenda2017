<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_cuotas_nif($no_factura,$nit,$fecha,$valor,$indice) 
{ 
	global $datos_impresion;
	$coneccion_ora=$_SESSION["coneccionora"];
	$cuota=0;
	$fecha_cuotas=array();
	$temp_cuota=$indice+1;
	$cursor_ora=$coneccion_ora->Execute("select CK_DIAS_INI_CTA,CK_PLAN_FINANC, CK_VAL_CUO_ACT ,  CK_CUOTAS_TOTAL,ACCOUNTING_DT  from SYSADM.PS_CK_CRM_CARTE_VW
	where ITEM='".$no_factura."'  and CUST_ID='".$nit."'");
	if($cursor_ora)
	{
		if(!$cursor_ora->EOF)
		{
			$periodicidad=$cursor_ora->fields["CK_DIAS_INI_CTA"];
			$fecha_inicio=$cursor_ora->fields["ACCOUNTING_DT"];
			if($cursor_ora->fields["CK_PLAN_FINANC"]=="CRE20MIN_MENS_DIC" || $cursor_ora->fields["CK_PLAN_FINANC"]=="CRE20MIN_QUIN_DIC")
			{
				$fecha_temporal=$datos_impresion->add_date($cursor_ora->fields['ACCOUNTING_DT'], 30);
				$fecha_inicio=$fecha_temporal["mysql"];
			}
			$plan=$cursor_ora->fields["CK_PLAN_FINANC"];
			$cuotas=$cursor_ora->fields["CK_CUOTAS_TOTAL"];

			for($i=1;$i<=$cuotas;$i++)
			{
				$fecha_temporal=$datos_impresion->add_date($fecha_inicio, $periodicidad*$i);
				$fecha_cuotas[$i]=$fecha_temporal["mysql"];

			}
			$sql1="select sum(ENTRY_AMT)  as total 
			from sysadm.PS_CK_CRM_COBNC_VW where INVOICE ='".$no_factura."'  and CUST_ID='".$nit."'
		 	and ENTRY_REASON!='ANTIC'  and ACCOUNTING_DT<='".$fecha."'";
			$cursor_nuevo=$coneccion_ora->Execute($sql1);
			$total_fecha=0;
			if($cursor_nuevo)
			{
				if(!$cursor_nuevo->EOF)
				{
					$total_fecha=abs($cursor_nuevo->fields["TOTAL"]);
				}
			}
			$valor_nuevo=$cursor_ora->fields['CK_VAL_CUO_ACT'];
			$maximo_valor=intval($cursor_ora->fields['CK_VAL_CUO_ACT'])*intval($cursor_ora->fields['CK_CUOTAS_TOTAL']);
			if($indice==0)
			{
			    $cuota=1;
			}
			else
			{
			   $divisor=$total_fecha/intval($cursor_ora->fields['CK_VAL_CUO_ACT']);

			   $residuo=$total_fecha%intval($cursor_ora->fields['CK_VAL_CUO_ACT']);
			   if($residuo>0 && intval($divisor)<2)
			   {
			   	$cuota=intval($divisor)+1;
			   }
			   else
			   {
				$cuota=round($divisor);
			   }
			}

			
		}
		
	}
	return $cuota;
} 
?>