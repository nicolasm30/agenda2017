<?php /* Smarty version 2.6.19, created on 2017-05-19 20:51:22
         compiled from index.tpl */ ?>
<html> 
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<body>
       
            <fieldset>
                <legend>Registro Usuarios</legend>
                <div class="container">
                    <div class="row">
                        <div class="form-group">
                            <label  class="col-md-2 control-label">Identificación</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="id" placeholder="Identificación">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-md-2 control-label">Nombres</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="nombres" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-md-2 control-label">Edad</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="edad" placeholder="Edad">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-md-2 control-label">Correo</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="correo" placeholder="Correo">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Genero</label>
                            <div class="col-md-10">
                                <select class="form-control" id="genero">
                                    <option value="1">HOMBRE</option>
                                    <option value="2">MUJER</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <button id="register" class="btn btn-primary">Registrar Usuario</button>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        <table id="tabla" style="width: 100%">
            <th> Identificacion</th>
            <th> Nombres </th>
            <th> Correo </th>
            <th> Edad </th>
            <th> Genero</th>
            
            <tbody>
                
                
            </tbody>
            
        </table>
        
    </body>
</html>