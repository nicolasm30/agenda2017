<head>
    <link href="lib/DataTables-1.10.15/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="lib/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <script src="lib/bootstrap-3.3.7-dist/js/bootstrap.js" type="text/javascript"></script>
    <script src="lib/jquery/jquery-3.2.1.js" type="text/javascript"></script>
    <script src="js/Scripts.js" type="text/javascript"></script>
    <script src="lib/DataTables-1.10.15/media/js/jquery.dataTables.js" type="text/javascript"></script>
    <meta charset="UTF-8">
    <title>Mi Agenda</title>
</head>
